# README #

The is a Cordova plugin designed to be able to get profile information from the user, if the data exists.

### How do I get set up? ###

Setting up is as easy as any other plugin:

```
#!sh

cordova plugin add https://bitbucket.org/sixgreen/cordova-plugin-myprofile
```


* Configuration


```
#!javascript


// Both profile and thumbnail calls are asyncronous.
 if (window.plugins.myprofile) {
    // XXX the profile call has no arguments.
    var options = {};

    window.plugins.myprofile.profile(options, function myProfileListener(data) {
        var profileData = JSON.stringify(data, undefined, 2);
        ... Do stuff with the data ...
    });
	
	// You can get the Base64 encoded JPEG thumbnail data using the url returned with:
	var thumbnailOptions = {
		"uri":"URI_FROM_PROFILE_CALL_ABOVE"
	};
    window.plugins.myprofile.thumbnail(thumbnailOptions, function myThumbnailListener(imageData) {
		var image = document.getElementById('myImage');
	    image.src = "data:image/jpeg;base64," + imageData;
    });
}
```


* Data Output

```
#!javascript

{
   "id":"9223372034707292161",
   "display_name":"John Doe",
   "phone":[
      {
         "type":"Mobile",
         "label":"",
         "number":"1 (555) 555-5555"
      }
   ],
   "lookupKey":"profile",
   "photo_thumb_uri":"content:\/\/com.android.contacts\/contacts\/9223372034707292161\/photo",
   "postal":[
      {
         "region":"Ontario",
         "formatted":"12 Dunlop St. E.\nBarrie, Ontario,\nCanada L4M 1A3",
         "postalcode":"L4M 1A3",
         "street":"12 Dunlop St E",
         "pobox":"",
         "longitude":"-79.689632",
         "label":"",
         "latitude":"44.389312",
         "type":"Home",
         "country":"Canada",
         "city":"Barrie"
      }
   ],
   "email":[
      {
         "type":"Work",
         "address":"yourname@example.com",
         "label":""
      }
   ]
}
```


### Contribution guidelines ###

If you want to add something to the project or make a fix, simply clone it, make your changes, and generate a pull request.

Of course, tests are welcome with new code.